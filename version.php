<?php
/**
 * @package		theme_cugeneral
 * @copyright	2016 Cornerstone University, www.cornerstone.edu
 * @author 		Trevor McCready
 * @license 	All rights reserved.
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version	=	2017010900;
$plugin->requires	=	2015051105; // Developed and tested for Moodle 2.9+
$plugin->release	=	'v3.1';
$plugin->maturity	=	MATURITY_BETA;
$plugin->component	=	'theme_cugeneral';