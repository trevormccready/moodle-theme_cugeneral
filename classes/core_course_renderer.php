<?php
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . "/course/renderer.php");

class theme_cuabts_core_course_renderer extends core_course_renderer {

    public function course_category($category) {
        global $CFG, $PAGE, $USER;
        require_once($CFG->libdir. '/coursecatlib.php');
        $context = context_system::instance();
        if (!is_siteadmin($USER->id) ||
        		!has_capability('moodle/category:viewhiddencategories', $context) ) {
        	$themecoursecategories = $PAGE->theme->settings->coursecategories;
        	$themecategoryids = explode(',',$themecoursecategories);
        	if (! $category) {
        	    $category = $themecategoryids[0];
        	}
        }
        $coursecat = coursecat::get(is_object($category) ? $category->id : $category);
        $site = get_site();
        $output = '';

        $this->page->set_button($this->course_search_form('', 'navbar'));
        if (!$coursecat->id) {
            if (can_edit_in_category()) {
                // add 'Manage' button instead of course search form
                $managebutton = $this->single_button(new moodle_url('/course/management.php'), get_string('managecourses'), 'get');
                $this->page->set_button($managebutton);
            }
            if (coursecat::count_all() == 1) {
                // There exists only one category in the system, do not display link to it
                $coursecat = coursecat::get_default();
                $strfulllistofcourses = get_string('fulllistofcourses');
                $this->page->set_title("$site->shortname: $strfulllistofcourses");
            } else {
                $strcategories = get_string('categories');
                $this->page->set_title("$site->shortname: $strcategories");
            }
        } else {
            $this->page->set_title("$site->shortname: ". $coursecat->get_formatted_name());
			
			// Get the category trees for each of the theme's visible categories
			$visiblecategories = array();
			$allcategoryitems = coursecat::make_categories_list(null, '');
			if (!is_siteadmin($USER->id) ||
             	!has_capability('moodle/category:viewhiddencategories', $context) ) {
				foreach ($themecategoryids as $themecategoryid) {
					$themecategory[$themecategoryid] = coursecat::get($themecategoryid);
				}
				foreach($allcategoryitems as $categoryid => $categoryitem) {
					$catpath = array();
					$catpath = explode(" / ", $categoryitem);
					foreach($themecategoryids as $themecategoryid) {
						if($catpath[0] == $themecategory[$themecategoryid]->name) {
							$visiblecategories[$categoryid] = $categoryitem;	
						}
					}
				}
			} else {
				$visiblecategories = $allcategoryitems;
			}
			// Print the category selector if there is more than one category visible to the user
			if(count($visiblecategories)>1) {
				$output .= html_writer::start_tag('div', array('class' => 'categorypicker'));
				$select = new single_select(new moodle_url('/course/index.php'), 'categoryid',
                    $visiblecategories, $coursecat->id, null, 'switchcategory');
				$select->set_label(get_string('categories').':');
				$output .= $this->render($select);
				$output .= html_writer::end_tag('div'); // .categorypicker
			}
        }

        // Print current category description
        $chelper = new coursecat_helper();
        if ($description = $chelper->get_category_formatted_description($coursecat)) {
            $output .= $this->box($description, array('class' => 'generalbox info'));
        }

        // Prepare parameters for courses and categories lists in the tree
        $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_AUTO)
                ->set_attributes(array('class' => 'category-browse category-browse-'.$coursecat->id));

        $coursedisplayoptions = array();
        $catdisplayoptions = array();
        $browse = optional_param('browse', null, PARAM_ALPHA);
        $perpage = optional_param('perpage', $CFG->coursesperpage, PARAM_INT);
        $page = optional_param('page', 0, PARAM_INT);
        $baseurl = new moodle_url('/course/index.php');
        if ($coursecat->id) {
            $baseurl->param('categoryid', $coursecat->id);
        }
        if ($perpage != $CFG->coursesperpage) {
            $baseurl->param('perpage', $perpage);
        }
        $coursedisplayoptions['limit'] = $perpage;
        $catdisplayoptions['limit'] = $perpage;
        if ($browse === 'courses' || !$coursecat->has_children()) {
            $coursedisplayoptions['offset'] = $page * $perpage;
            $coursedisplayoptions['paginationurl'] = new moodle_url($baseurl, array('browse' => 'courses'));
            $catdisplayoptions['nodisplay'] = true;
            $catdisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'categories'));
            $catdisplayoptions['viewmoretext'] = new lang_string('viewallsubcategories');
        } else if ($browse === 'categories' || !$coursecat->has_courses()) {
            $coursedisplayoptions['nodisplay'] = true;
            $catdisplayoptions['offset'] = $page * $perpage;
            $catdisplayoptions['paginationurl'] = new moodle_url($baseurl, array('browse' => 'categories'));
            $coursedisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'courses'));
            $coursedisplayoptions['viewmoretext'] = new lang_string('viewallcourses');
        } else {
            // we have a category that has both subcategories and courses, display pagination separately
            $coursedisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'courses', 'page' => 1));
            $catdisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'categories', 'page' => 1));
        }
        $chelper->set_courses_display_options($coursedisplayoptions)->set_categories_display_options($catdisplayoptions);

        // Display course category tree
        
        
        $output .= $this->coursecat_tree($chelper, $coursecat);

        // Add course search form (if we are inside category it was already added to the navbar)
        if (!$coursecat->id) {
            $output .= $this->course_search_form();
        }

        // Add action buttons
        $output .= $this->container_start('buttons');
        $context = get_category_or_system_context($coursecat->id);
        if (has_capability('moodle/course:create', $context)) {
            // Print link to create a new course, for the 1st available category.
            if ($coursecat->id) {
                $url = new moodle_url('/course/edit.php', array('category' => $coursecat->id, 'returnto' => 'category'));
            } else {
                $url = new moodle_url('/course/edit.php', array('category' => $CFG->defaultrequestcategory, 'returnto' => 'topcat'));
            }
            $output .= $this->single_button($url, get_string('addnewcourse'), 'get');
        }
        ob_start();
        if (coursecat::count_all() == 1) {
            print_course_request_buttons(context_system::instance());
        } else {
            print_course_request_buttons($context);
        }
        $output .= ob_get_contents();
        ob_end_clean();
        $output .= $this->container_end();

        return $output;
    }
    
    function course_search_form($value = '', $format = 'plain') {
    	static $count = 0;
    	$formid = 'coursesearch';
    	if ((++$count) > 1) {
    		$formid .= $count;
    	}
    
    	switch ($format) {
    		case 'navbar' :
    			$formid = 'coursesearchnavbar';
    			$inputid = 'navsearchbox';
    			$inputsize = 15;
    			break;
    		case 'short' :
    			$inputid = 'shortsearchbox';
    			$inputsize = 12;
    			break;
    		default :
    			$inputid = 'coursesearchbox';
    			$inputsize = 20;
    	}
    
    	$strsearchcourses= get_string("searchcourses");
    	$searchurl = new moodle_url('/course/search.php');
    
    	$output = html_writer::start_tag('form', array('id' => $formid, 'action' => $searchurl, 'method' => 'get'));
    	$output .= html_writer::start_tag('fieldset', array('class' => 'coursesearchbox invisiblefieldset'));
    	//$output .= html_writer::tag('label', $strsearchcourses.': ', array('for' => $inputid));
    	$output .= html_writer::empty_tag('input', array('type' => 'text', 'id' => $inputid,
    			'size' => $inputsize, 'name' => 'search', 'value' => s($value)));
    	$output .= html_writer::empty_tag('input', array('type' => 'submit',
    			'value' => get_string('searchcourses')));
    	$output .= html_writer::end_tag('fieldset');
    	$output .= html_writer::end_tag('form');
    
    	return $output;
    }
    
    public function search_courses($searchcriteria) {
    	global $CFG, $DB, $PAGE, $USER;
    	$content = '';
    	if (!empty($searchcriteria)) {
    		// print search results
    		require_once($CFG->libdir. '/coursecatlib.php');
    
    		$displayoptions = array('sort' => array('displayname' => 1));
    		// take the current page and number of results per page from query
    		$perpage = optional_param('perpage', 0, PARAM_RAW);
    		if ($perpage !== 'all') {
    			$displayoptions['limit'] = ((int)$perpage <= 0) ? $CFG->coursesperpage : (int)$perpage;
    			$page = optional_param('page', 0, PARAM_INT);
    			$displayoptions['offset'] = $displayoptions['limit'] * $page;
    		}
    		// options 'paginationurl' and 'paginationallowall' are only used in method coursecat_courses()
    		$displayoptions['paginationurl'] = new moodle_url('/course/search.php', $searchcriteria);
    		$displayoptions['paginationallowall'] = true; // allow adding link 'View all'
    
    		$class = 'course-search-result';
    		foreach ($searchcriteria as $key => $value) {
    			if (!empty($value)) {
    				$class .= ' course-search-result-'. $key;
    			}
    		}
    		$chelper = new coursecat_helper();
    		$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT)->
    		set_courses_display_options($displayoptions)->
    		set_search_criteria($searchcriteria)->
    		set_attributes(array('class' => $class));
    		
    		$courses = coursecat::search_courses($searchcriteria, $chelper->get_courses_display_options());
    		$allcourses = coursecat::search_courses($searchcriteria);
    		$totalcount = coursecat::search_courses_count($searchcriteria);
            		
    	    $context = context_system::instance();
    		if (!is_siteadmin($USER->id) ||
    				!has_capability('moodle/category:viewhiddencategories', $context) ) {
    			$themecoursecategories = $PAGE->theme->settings->coursecategories;
    			$themecategoryids = explode(',',$themecoursecategories);
    			$options['idonly'] = true;
    			$allcourseids = coursecat::search_courses($searchcriteria,$options);
    			$newtotalcount = count($allcourseids);
    			foreach($allcourseids as $key) {
    			    $keycourse = $DB->get_record('course',array('id'=>$key));
    			    $parentcategoryids = coursecat::get($keycourse->category)->get_parents();
    			    foreach($themecategoryids as $themecategoryid) {
    			        if(!in_array($themecategoryid,$parentcategoryids)) {
    			            unset($allcourses[$key]);
    			            if(isset($courses[$key])) {
    			                unset($courses[$key]);
    			            }
    			            if(($akey = array_search($key, $allcourseids)) !== false) {
    			                unset($allcourseids[$akey]);
    			            }
    			        }			        
    			    }
    			}
    			$totalcount = count($allcourseids);
    			$courseslist = $this->courses_list($allcourses,true,$class,$displayoptions['paginationurl'],$totalcount,$page,$perpage);
    		} else {
    		    $courseslist = $this->coursecat_courses($chelper, $courses, $totalcount);
    		}
    		
    		if (!$totalcount) {
    			if (!empty($searchcriteria['search'])) {
    				$content .= $this->heading(get_string('nocoursesfound', '', $searchcriteria['search']));
    			} else {
    				$content .= $this->heading(get_string('novalidcourses'));
    			}
    		} else {
    			$content .= $this->heading(get_string('searchresults'). ": $totalcount");
    			$content .= $courseslist;
    		}
    
    		if (!empty($searchcriteria['search'])) {
    			// print search form only if there was a search by search string, otherwise it is confusing
    			$content .= $this->box_start('generalbox mdl-align');
    			$content .= $this->course_search_form($searchcriteria['search']);
    			$content .= $this->box_end();
    		}
    	} else {
    		// just print search form
    		$content .= $this->box_start('generalbox mdl-align');
    		$content .= $this->course_search_form();
    		$content .= html_writer::tag('div', get_string("searchhelp"), array('class' => 'searchhelp'));
    		$content .= $this->box_end();
    	}
    	return $content;
    }

}