<?php
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . "/blocks/navigation/renderer.php");

class theme_cuabts_block_navigation_renderer extends block_navigation_renderer {
	public function navigation_tree(global_navigation $navigation, $expansionlimit, array $options = array()) {
		global $PAGE, $USER;
		
		// Remove 'Site Pages' branch in the navigation block
		$sitepagesnode=$navigation->find('site', navigation_node::TYPE_ROOTNODE);
		if($sitepagesnode){
			$sitepagesnode->remove();
			$navigation = $sitepagesnode->parent;
		}
		
		// Remove the Courses node in the Navigation block from all, except sysadmins and users with the ability to see hidden categories
		$context = context_system::instance();
		if (!is_siteadmin($USER->id) ||
             !has_capability('moodle/category:viewhiddencategories', $context) ) {
			
			// Remove the Moodle-generated 'Courses' listings in the navigation block
			$coursenode=$navigation->find('courses', navigation_node::TYPE_ROOTNODE);
			if($coursenode){
				$coursenode->remove();
				$navigation = $coursenode->parent;	
			} 
			
			// Add only the course categories the user should see while using this theme
			if(!empty($PAGE->theme->settings->coursecategories)) {
				$containernodeurl = '/course/index.php';
				$coursecategories = $PAGE->theme->settings->coursecategories;
				$categories = explode(',',$coursecategories);
				if(count($categories)>1) {
					$containernode = $PAGE->navigation->add(get_string('courses'), null, navigation_node::TYPE_CONTAINER);
					foreach ($categories as $category) {
						$categorynodeurl = $containernodeurl . '?categoryid=' . $category;
						$label = $this->get_label($category);
						$categorynode = $containernode->add($label, new moodle_url($categorynodeurl), navigation_node::TYPE_CONTAINER);
					}
				} else {
					$containernodeurl .= '?categoryid=' . $categories[0];
					$label = $this->get_label($categories[0]);
					$containernode = $PAGE->navigation->add($label, $containernodeurl, navigation_node::TYPE_CONTAINER);
				}
			}
		}
        return parent::navigation_tree($navigation, $expansionlimit,$options); 
    }
	
	function get_label($categoryid) {
		global $CFG;
		require_once($CFG->dirroot . "/lib/coursecatlib.php");		
		$label = get_string('configtitle','theme_cuabts') . ' ' . get_string('courses');
		
		$category = coursecat::get($categoryid);
		$label = $category->name . ' Courses';
		
		return $label;
	}
	
	public function navigation_node($items, $attrs=array(), $expansionlimit=null, array $options = array(), $depth=1) {
	    global $USER, $PAGE, $CFG;

	    // Remove just the course categories that aren't part of the theme
	    $coursecategories = $PAGE->theme->settings->coursecategories;
	    $categories = explode(',',$coursecategories);
	    foreach($categories as $category) {
	        $categorynames[] = $this->get_label($category);
	    }
	    foreach($items as $item) {
	        if($item->type == navigation_node::TYPE_CATEGORY && ! in_array($item->text,$categorynames)){
	            $item->remove();
	        }
	    }
	    return parent::navigation_node($items, $attrs, $expansionlimit, $options, $depth);
	}
			 
}