<?php
$settings = null;

defined('MOODLE_INTERNAL') || die;

	global $PAGE;

	$ADMIN->add('themes', new admin_category('theme_cugeneral', 'NON-cornerstone.edu'));
	
	// IDENTITY OPTIONS
	$temp = new admin_settingpage('theme_cugeneral_org',  'Identity');
    // Organization Name
	$name = 'theme_cugeneral/orgname';
    $title = 'Organization Name';
    $description = 'Default display name of this organization.';
    $default = 'Non-CU Account Users';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_CLEAN, 30);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);  
    
    $name = 'theme_cugeneral/website';
    $title = 'Website URL';
    $description = 'Fully-qualified domain name with path to website';
    $default = 'https://www.cornerstone.edu/login/general';
    $setting = new admin_setting_configtext($name,$title,$description,$default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Visible Course Categories
    $name = 'theme_cugeneral/coursecategories';
    $title = get_string('coursecategories','theme_cugeneral');
    $description = get_string('coursecategoriesdesc', 'theme_cugeneral');
    $default = 0;
    $description = $description . '<br/><a href="' . $CFG->wwwroot . '/course/management.php" target="_blank">View the Moodle Course Manager <i class="fa fa-external-link"></i></a>';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_CLEAN, 30);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_cugeneral',$temp);
    
    // NAVIGATION OPTIONS
    $temp = new admin_settingpage('theme_cugeneral_nav',  'Navigation');     
    /* Resources Navigation Menu Dropdown */
    $name = 'theme_cugeneral/showresourcesdropdown';
    $title = 'Show Resources Menu';
    $description = 'Display the Resources dropdown menu in the site navigation.';
    $default = true;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
  
    $temp->add(new admin_setting_heading('theme_cugeneral_nav','Quick Links',format_text('Links to portal and email'), FORMAT_MARKDOWN));
    /* Portal Link and URL */
    $name = 'theme_cugeneral/showportallink';
    $title = 'Show Portal Link';
    $description = 'Display a link to the Portal in the site navigation.';
    $default = true;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_cugeneral/portalpath';
    $title = 'Portal URL';
    $description = 'Fully-qualified domain name with path to relevant portal page';
    $default = 'https://portal.cornerstone.edu';
    $setting = new admin_setting_configtext($name,$title,$description,$default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Email Link and URL */
    $name = 'theme_cugeneral/showemaillink';
    $title = 'Show Email Link';
    $description = 'Display a link to email in the site navigation.';
    $default = true;
    $setting = new admin_setting_configcheckbox($name,$title,$description,$default,true,false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_cugeneral/emailpath';
    $title = 'Email URL';
    $description = 'Fully-qualified domain name with path to relevant email page';
    $default = 'http://gmail.cornerstone.edu';
    $setting = new admin_setting_configtext($name,$title,$description,$default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_cugeneral',$temp);