<?php
$string['pluginname']	=	'Non-Cornerstone.edu Account Theme';
$string['configtitle'] = 'CU GENERAL';
$string['region-side-post']	=	'Right';
$string['region-side-pre']	=	'Left';
$string['choosereadme']	=	'
		<div class="clearfix">
			<h2>Cornerstone University</h2>
			<h3>Theme Credits</h3>
			<p>This theme was developed by <a href="http://trevormccready.info" title="Trevor McCready" target="_blank">Trevor McCready</a> under the employ of <a href="https://www.cornerstone.edu" target="_blank" title="Cornerstone University">Cornerstone University</a>.</p>
		</div>';
$string['coursecategories'] = 'Course Categories';
$string['coursecategoriesdesc'] = 'A comma-seperated list of top-level course categories that should be displayed (if marked as visible) for everyone using this theme.<br/>Course Category IDs can be found by looking at the categoryID parameter listed in the URLs for the categories listed in the Moodle course manager.';