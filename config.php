<?php
$THEME->doctype =	'html5';
$THEME->name	= 	'cugeneral';
$THEME->parents	= 	array('cornerstone');
$THEME->sheets	=	array('general');
$THEME->javascripts	=	array();
$THEME->javascripts_footer	=	array();
$THEME->rendererfactory 	=	'theme_overridden_renderer_factory';
$THEME->enable_dock = true;